#  JS 获取浏览器及系统版本

> 这并不是一个正规的库，只是个人的学习 DEMO，并没有经过完全的系统的测试。
## Usage
```js
import getDeviceInfo from './lib/index.js';

const deviceInfo = getDeviceInfo();

console.log(deviceInfo);
// output:
{ 
  osName: string,
  osVersion: string, 
  browserName: string, 
  browserVersion: string, 
  isMobile: boolean 
}
```
## Reference

[🔗 获取系统和浏览器信息](https://www.wolai.com/wentao/wEGgmAvpa5gFPXEAoAAECr)

## Preview

![](https://secure2.wostatic.cn/static/pJSLXK8WbzMhh6NGDJoedX/image.png?auth_key=1680594047-56xDaDtBpotP4FkrhmD4cn-0-aa5f4e3b33c27cec2bd2aa29972b97fd&image_process=resize,w_1218&file_size=40142)

## Updates

1. navigator.userAgentData
<br/>新的 API 中可以获取到更准确的信息, 但目前只有 Chrome/Edge 支持。
```js
navigator.userAgentData;

// 在 Microsoft Edge 中的输出结果:
{
  brands: [
    { brand: 'Chromium', version: '112' },
    { brand: 'Microsoft Edge', version: '112' },
    { brand: 'Not:A-Brand', version: '99' }
  ],
  mobile: false,
  platform: 'macOS'
};
```


## Known issues
1. 在 iPad 的 Safari 中不能正确识别
- 至少我的 M1 芯片的 iPad 存在这个问题

- 因为其 userAgent 和 MacOS 完全相同, 所以会错误地识别为 MacOS

2. 不能正确识别 Windows 11
- 因为 Windows11 并未更新 userAgent，仍然是 Windows NT 10.0。

- 尽管可以通过 `navigator.userAgentData.getHighEntropyValues(["platformVersion"])` 获取 platformVersion 来判断，但最新的 Windows 11 22H2 中 Edge 浏览器中并没有该 API 的实现。

3. 不能完全正确识别 MacOS
 - 如果浏览器支持 `navigator.userAgentData.getHighEntropyValues(["platformVersion"])` 则能够正确识别版本号。
 - 否则仅通过 userAgent 无法准确识别 MacOS 的版本，因为始终是 `Mac OS X 10_15_7`
