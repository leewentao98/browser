import browsers from './browsers.js';
import systems from './systems.js';

/**
 * @description 从 brands 中获取浏览器, 目前仅 Chrome/Edge 支持
 * @param {Array} brands 
 * @returns {string}
 */
const getBrowserNameByBrands = (brands) => {
  const names = brands.map((b) => b.brand.toLowerCase());
  if (names.includes('google chrome')) {
    return 'Chrome';
  }
  if (names.includes('microsoft edge')) {
    return 'Microsoft Edge';
  }
  return '';
};

/**
 * @description 获取操作系统信息
 * @returns {Object}
 */
const getOSInfo = () => {
  const { userAgent } = navigator;
  const osInfo = systems.find(({ identifier }) => identifier.some((id) => userAgent.includes(id)));
  const osVersion = osInfo.getVersion(userAgent);
  return {
    osName: osInfo.name,
    osVersion
  };
};
/**
 * @description 获取浏览器信息
 * @returns {Object}
 */
const getBrowserInfo = () => {
  const { userAgent } = navigator;
  const browserInfo = browsers.find(({ identifier }) => identifier.some((i) => userAgent.includes(i)));
  const browserVersion = browserInfo.getVersion(userAgent); // 系统名称

  return {
    browserName: browserInfo.name,
    browserVersion
  };
};

/**
 * @description 判断是否移动设备
 * @returns {Boolean} true: 移动端 & false: PC 端
 */
const isMobile = () => {
  const isMatch = !!window.navigator.userAgent.match(
    /(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i
  );
  return isMatch;
};

/**
 * @description 获取全部设备信息
 * @returns {Object}
 */
const getDeviceInfo = async () => {
  const { userAgentData } = navigator;

  if (userAgentData && userAgentData.getHighEntropyValues) {
    const {
      platform: osName,
      platformVersion: osVersion,
      brands,
      uaFullVersion: browserVersion,
      mobile: isMobile
    } = await userAgentData.getHighEntropyValues(['platform', 'platformVersion', 'uaFullVersion']);

    return { osName, osVersion, browserName: getBrowserNameByBrands(brands), browserVersion, isMobile };
  }

  return {
    ...getOSInfo(),
    ...getBrowserInfo(),
    isMobile: isMobile()
  };
};

export { getDeviceInfo as default, getOSInfo, getBrowserInfo, isMobile };
